//
//  ViewController.swift
//  EdurekaModule6a
//
//  Created by Jay on 16/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    //lets get an outlet for the image view
    @IBOutlet var imageView: UIImageView!
    


    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("viewDidLoad loaded")
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //lets get an action for taking photos
        //make sure capabilities are set before trying any of the photo things
    @IBAction func takePicture(_ sender: UIBarButtonItem)
    {
        print("takePicture begins")
        
        let imagePicker = UIImagePickerController()
        
        //check if phone has camera.
        if UIImagePickerController.isSourceTypeAvailable(.camera)
        {
            print("takePicture - Camera is available")
            
            //Check the source has the camera on the phone
            imagePicker.sourceType = .camera
        }
        else
        {
            print("takePicture - Camera is NOT available")
        }
        
        //after an image has been taken (or picked) a number of events will be fire.
        //so we need to collect them in a delegate.
        
        imagePicker.delegate = self
        
        //lets present the photo.
        present(imagePicker,animated: true,completion: nil)
        
        print("takePicture ends")
    }
    
    //lets write a function to pick photo from album
    @IBAction func takePhoto(_ sender: UIBarButtonItem)
    {
        print("takePhoto begins")
        
        let imagePicker = UIImagePickerController()
        
        //check if phone has camera.
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        {
            print("takePhoto - Photo Library is available")
            
            //Check the source has the camera on the phone
            imagePicker.sourceType = .photoLibrary
        }
        else
        {
            print("takePhoto - Photo Library is NOT available")
        }
        
        //after an image has been taken (or picked) a number of events will be fire.
        //so we need to collect them in a delegate.
        
        imagePicker.delegate = self
        
        //lets present the photo.
        present(imagePicker,animated: true,completion: nil)
        
        print("takePhoto ends")
    }
    
    //lets write a method to handle the photo that is selected.
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        print("imagePickerController begins")
        
        //grab the image from the controller
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        //okay, now we got the image, lets put it for display man.
        imageView.image = image
        
        //lets save the image to the phone
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        print("imagePickerController - phot was saved.")
        
        //okay, lets stop looking at the image.
        dismiss(animated: true, completion: nil)
        
        print("imagePickerController ends")
    }
    
    //lets write a method that will handle the cancellation.
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        print("imagePickerControllerDidCancel begins")
        
        print("use cancelled the photo taking")
        dismiss(animated: true, completion: nil)
        
        print("imagePickerControllerDidCancel ends")
        
    }
}

